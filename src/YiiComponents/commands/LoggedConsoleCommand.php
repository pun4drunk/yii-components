<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */
    
/**
 * Description of FConsoleCommand
 *
 * @author vladislav
 */
namespace YiiComponents\commands;
use YiiComponents\interfaces\ILoggedConsoleCommand;

abstract class LoggedConsoleCommand extends ConsoleCommand implements ILoggedConsoleCommand {

    public $debug;
    public $logLive = true;
    public $autoFlush = 1;
    
    protected $_logCategory;
    
    protected function beforeAction($action,$params) {
        if ($this->logLive) {
        // automatically send every new message to available log routes
            \Yii::getLogger()->autoFlush = $this->autoFlush;
        // when sending a message to log routes, also notify them to dump the message
        // into the corresponding persistent storage (e.g. DB, email)
            \Yii::getLogger()->autoDump = true;
        }
        return parent::beforeAction($action, $params);
    }
    
    
    public function getLogger() {
        
        if (!$this->hasLogger()) {
            $this->attachBehavior('logger', $this->loggerBehavior);
        }
        
        return $this->asa('logger');
    }
    
    public function _exit($message) {
        if (!$this->debug && $this->asa('logger')) {
            $this->logger->addError($message);
            exit;
        } else {
            parent::_exit($message);
        }
    }
    
    protected function getLoggerBehavior() {
        return array(
            'class'     => 'YiiComponents\behaviors\LoggerBehavior',
            'debug'     => $this->debug,
            'category'  => $this->logCategory,
            'live'      => $this->logLive,
            'requireCategory' => false,
        );
    }
    
    public function hasLogger() {
        return $this->asa('logger');
    }
    
    public function setLogCategory($value) {
        $logCategory = $this->_logCategory;
        $this->_logCategory = $value;
        
        if ($logCategory !== $value) {
            if ($this->hasLogger()) {
                $this->logger->category = $this->logCategory;
            }
        }
        return $this;
    }
    
    public function getLogCategory() {
        return is_null($this->_logCategory) ? "application.$this->name" 
                                           : $this->_logCategory;
    }
    
}


