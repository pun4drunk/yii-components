<?php

/*
 * @category  Projects
 * @package   yii-components
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2015 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of ConfiguredConsoleCommand
 *
 * @author vladislav
 */
namespace YiiComponents\commands;

abstract class ConfiguredConsoleCommand extends LoggedConsoleCommand {
    
    public function beforeAction($action, $params) {
    
        if (!parent::beforeAction($action, $params)) {
            return false;
        }
        
        $this->attachSettings(array(
            'store' => 'setConfig'      === $action,
            'reset' => 'resetConfig'    === $action,
            'show'  => 'showConfig'     === $action,
        ))->merge();
        
        return true;
    }
    
    protected function attachSettings($params = array()) {
        
        $this->attachBehavior('settings', \CMap::mergeArray($this->settingsBehavior, $params));
        
        return $this->asa('settings');
    }
    
    protected function getSettingsBehavior() {
        return array(
            'class'         => 'YiiComponents\behaviors\EavPropertiesBehavior',
            'category'      => 'settings',
            'entity'        => $this->commandName,
            'attributes'    => $this->commandParams,
            'modelClass'    => 'Setting',
        );
    }
    
    public function getCommandName() {
        return $this->name;
    }
    
    protected function getCommandParams() {
        return array();
    }
    
    public function actionSetConfig() {
        return 1;
    }
    
    public function actionResetConfig() {
        return 1;
    }
    
    public function actionShowConfig() {
        return 1;
    }
}
