<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */
    
/**
 * Description of FConsoleCommand
 *
 * @author vladislav
 */
namespace YiiComponents\commands;
use CConsoleCommand;

abstract class ConsoleCommand extends CConsoleCommand {

    public $debug = false;
    
    public function run($args) {
        
        $result = 0;
        
        try {
            $result = parent::run($args);
        } catch (\Exception $e) {
            
            if ($this->debug) {
                throw $e;
            } else {
                $this->_exit($e->getMessage());
            }
        }
        
        return $result;
    }
    
    public function _exit($message) {
        echo "$message", PHP_EOL;
        exit;
    }
    
}


