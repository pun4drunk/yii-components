<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ActiveRecord
 *
 * @author vladislav
 */
namespace YiiComponents\models;
use YiiComponents\helpers\AppHelper;

abstract class ActiveRecord extends \CActiveRecord {
    
    public function behaviors() {
        
        $behaviors = array(
            'forceInsert' => array(
                'class' => 'YiiComponents\behaviors\ActiveRecordForceInsertBehavior',
            ),
        );
        
        if ($this->hasAttribute('created') || $this->hasAttribute('updated')) {
            $behaviors['timestamps'] = array(
                'class' => 'YiiComponents\behaviors\TimestampBehavior',
                'createAttribute' => $this->hasAttribute('created') ? 'created' : NULL,
                'updateAttribute' => $this->hasAttribute('updated') ? 'updated' : NULL,
                'timestampExpression' => AppHelper::getDbTimestampExpression($this->getDbConnection()),
            );
        }
        
        if ($this->hasAttribute('created')) {
            $behaviors['timedBehavior'] = array(
                'class' => 'YiiComponents\behaviors\ActiveRecordTimedBehavior'
            );
        }
        
        return $behaviors;
    }
    
    public function getSearchCriteria($mergeWith = array(), $operator = 'AND', $criteria = NULL) {
        $criteria = is_null($criteria) ? new \CDbCriteria() : $criteria;
        if ($mergeWith) {
            $criteria->mergeWith($mergeWith, $operator);
        }
        
        return $criteria;
    }

    
    protected function sortParams() {
        return array();
    }
    
    
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
            // @todo Please modify the following code to remove attributes that should not be searched.

            return new \CActiveDataProvider($this, array(
                    'criteria'=>$this->getSearchCriteria(),
                    'sort' => $this->sortParams(),
            ));
    }
    
    public function includeValues($column, array $values) {
        $this->getDbCriteria()->addInCondition($column, $values);
        return $this;
    }
    
    public function excludeValues($column, array $values) {
        $this->getDbCriteria()->addNotInCondition($column, $values);
        return $this;
    }
    
}
