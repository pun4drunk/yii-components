<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of CacheModel
 *
 * @author vladislav
 */
namespace YiiComponents\models;

abstract class CacheModel extends StorageModel {
    
    public $storageBehavior = array(
        'class' => 'YiiComponents\behaviors\CacheStorageBehavior',
    );
    public $uniqueIdKey = 'cacheKeyPrefix';
    public $uniqueIdPrefix;
    public $id;
    
    public function __construct($id = NULL) {
        $this->id = $id;
        $this->storageBehavior[$this->uniqueIdKey] = $this->uniqueIdPrefix ? "$this->uniqueIdPrefix.$id" : $id;
        
        parent::__construct($id);
        
    }
    
    public function getId() {
        return $this->id;
    }
    
}
