<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of StorageModel
 *
 * @author vladislav
 */
namespace YiiComponents\models;
use CModel;

abstract class StorageModel extends CModel { 
    
    protected $_attributes = array();
    abstract public function attributes();

    public $storageBehavior = array(
        //'class' => 'CacheStorageBehavior',
        //'cacheKeyPrefix' => $id,
    );
    public $storageInterface = 'YiiComponents\interfaces\IStorageBehavior';
    
    public function __construct($id = NULL) {
        
        $this->_attributes = $this->attributes();
        
        $this->attachBehavior('storage', $this->storageBehavior);
        
        if (!$this->storage instanceof $this->storageInterface) {
            throw new CException("StorageBehavior ({$this->storageBehavior['class']}) of ".get_class($this)." must implement $this->storageInterface");
        }
        
    }
    
    public function __get($name)
    {
        if(isset($this->_attributes[$name]))
            return $this->storage->get($name);
        else
            return parent::__get($name);
    }
    
    public function __set($name,$value)
    {
        if($this->setAttribute($name,$value)===false)
        {
            parent::__set($name,$value);
        }
    }
    
    public function __call($name, $parameters) {
        
        if (preg_match("~^delete([A-Z0-9]{1}\w+)~", $name, $matches)) {
            $attribute = lcfirst($matches[1]);
            return $this->unsetAttribute($attribute);
        } else if (preg_match("~^get([A-Z0-9]{1}\w+)~", $name,  $matches)) {
            $attribute = lcfirst($matches[1]);
            if (isset($this->_attributes[$attribute])) {
                return $this->$attribute;
            }
        } else if (preg_match("~^set([A-Z0-9]{1}\w+)~", $name,  $matches)) {
            $attribute = lcfirst($matches[1]);
            if (isset($this->_attributes[$attribute])) {
                return $this->setAttribute($attribute, reset($parameters));
            }
        }
        
        return parent::__call($name, $parameters);
    }
    
    public function setAttribute($name,$value)
    {
        if(property_exists($this,$name))
                $this->$name=$value;
        elseif(isset($this->_attributes[$name]))
                $this->storage->set($name, $value);
        else
                return false;
        
        return true;
    }
    
    public function unsetAttribute($name) {
        return $this->storage->reset($name);
    }
    
    public function unsetAttributes($names = null) {
        if($names===null)
                $names=$this->attributeNames();
        foreach($names as $name)
                $this->unsetAttribute($name);
    }
    
    public function attributeNames() {
        return array_keys($this->_attributes);
    }
}
