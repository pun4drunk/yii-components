<?php
namespace YiiComponents\helpers;

abstract class ArrayHelper {
        
    public static function get($name, &$array, $default = NULL, $unset = false){
        
        if (is_array($name)) {
            $values = array();
            foreach ($name as $_name => $_default) {
                array_push($values, self::get($_name, $array, $_default, $unset));
            }
            return $values;
        }
        
        $result = $default;    
        if(isset($array[$name])){
            $result = $array[$name];
            if($unset) unset($array[$name]);
        }
        
        return $result;
    }
    
    public static function extract($name, &$array, $default = NULL){
        return self::get($name, $array, $default, true);
    }
    
}
