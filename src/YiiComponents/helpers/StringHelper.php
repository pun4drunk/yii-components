<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StringHelper
 *
 * @author vladislav
 */
namespace YiiComponents\helpers;

abstract class StringHelper {
    
    public static function truncate($string, $maxLength, $append = "...") {
        return mb_strlen($string) <= $maxLength ? $string : mb_substr($string, 0, $maxLength).$append;
    }
    
}
