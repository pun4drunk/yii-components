<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of AppHelper
 *
 * @author vladislav
 */
namespace YiiComponents\helpers;
use YiiComponents\interfaces\IDbHelper;

abstract class AppHelper implements IDbHelper {
    
    public static function isConsoleApp() {
        return \Yii::app() instanceof \CConsoleApplication;
    }
    
    public static function generateLabel($name) {
        return ucwords(trim(strtolower(str_replace(array('-','_','.'),' ',preg_replace('/(?<![A-Z])[A-Z]/', ' \0', $name)))));
    }
    
    public static function implodeWrap($glue, $pieces, $wrap = "'") {
        return $wrap.implode("$wrap{$glue}$wrap", $pieces).$wrap;
    }
    
    public static function formatBytes($size,$level=0,$precision=2,$base=1024) 
    {
        $unit = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB','YB');
        $times = floor(log($size,$base));
        return sprintf("%.".$precision."f",$size/pow($base,($times+$level)))." ".$unit[$times+$level];
    }
    
    public static function getDbTimestamp($timestamp, $db = 'db', $reverse = false) {
        
        switch (static::getDbType($db)) {
            case static::SQLITE:
                $timestamp = $reverse ? date("Y-m-d H:i:s", $timestamp) : strtotime($timestamp);
                break;
            default:
                break;
        }
        
        return $timestamp;
    }
    
    public static function getDbTimestampLocal($timestamp, $db = 'db') {
        return $timestamp ? static::getDbTimestamp($timestamp, $db) + date("Z") : $timestamp;
    }
    
    public static function getDbTimestampExpression($db = 'db', $timestamp = NULL) {
        $result = is_null($timestamp) ? 'NOW()' : $timestamp;
        switch (static::getDbType($db)) {
            case static::SQLITE:
                $result = new \CDbExpression(is_null($timestamp) ? "CURRENT_TIMESTAMP" : "DATETIME($timestamp, 'unixepoch')");
                break;
            default:
                break;
        }
        return $result;
    }
    
    public static function getDbType($db = 'db') {
        if (is_string($db)) {
            $db = \Yii::app()->getComponent($db);
        }
        list($type) = explode(':', \Yii::app()->db->connectionString);
        return $type;
    }
}
