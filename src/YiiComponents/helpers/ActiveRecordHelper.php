<?php
namespace YiiComponents\helpers;

abstract class ActiveRecordHelper {
    
    protected static $_primaryKeys;
    protected static $_tableNames;
    protected static $_titleFields;
    
    public static function getPk($className){
        if(!ArrayHelper::get($className, self::$_primaryKeys)){
            self::$_primaryKeys[$className] = \CActiveRecord::model($className)->tableSchema->primaryKey;
        }
        return self::$_primaryKeys[$className];        
    }
    
    public static function getTableName($className){
        if(!ArrayHelper::get($className, self::$_tableNames)){
            self::$_tableNames[$className] = \CActiveRecord::model($className)->tableSchema->name;
        }
        return self::$_tableNames[$className];        
    }
    
    public static function getTitleField($className){
        if (!ArrayHelper::get($className, self::$_titleFields)){
            self::$_titleFields[$className] = \CActiveRecord::model($className)->titleField;
        }
        return self::$_titleFields[$className];
    }
    
}
