<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ConsoleHelper
 *
 * @author Vladislav
 */
namespace YiiComponents\helpers;

class ConsoleHelper {
    
    public static function getExpression($name, $action = "", $args = array()) {
        $result = "yiic $name $action";
        foreach ($args as $name => $value) {
            $result.=" --$name=$value";
        }
        return escapeshellcmd($result);
    }
    
}
