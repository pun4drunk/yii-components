<?php

/*
 * @category  Projects
 * @package   self.socialFella.reborn
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2015 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 *
 * @author vladislav
 */
namespace YiiComponents\interfaces;

interface IRestorableActiveRecord {
    public function restoreByAttributes(array $attributes, $autoCreate = false);
}

