<?php

/*
 * @category  Projects
 * @package   yii-components
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2015 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 *
 * @author vladislav
 */
namespace YiiComponents\interfaces;

interface ILoggedConsoleCommand {
    
    public function getLogger();
    public function getLogCategory();
    public function setLogCategory($value);

}
