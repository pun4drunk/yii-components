<?php

/*
 * @category  Projects
 * @package   yii-components
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2015 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 *
 * @author vladislav
 */
namespace YiiComponents\interfaces;

interface IDbHelper {
    
    const SQLITE    = 'sqlite';
    const MYSQL     = 'mysql';
    
    public static function getDbType($db = 'db');
    public static function getDbTimestamp($timestamp, $db = 'db');    
    public static function getDbTimestampLocal($timestamp, $db = 'db');
    public static function getDbTimestampExpression($db = 'db');

}
