<?php

/*
 * @category  Projects
 * @package   self.socialFella.reborn
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2015 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of ActiveRecordRestorableBehavior
 *
 * @author vladislav
 */

namespace YiiComponents\behaviors;
use YiiComponents\interfaces\IRestorableActiveRecord;
use \CBehavior;

class ActiveRecordRestorableBehavior extends CBehavior implements IRestorableActiveRecord {
    
    public function createByAttributes(array $attributes) {
        $className = get_class($this->owner);
        $new = new $className;
        $new->attributes = $attributes;
        if (!$new->save()) {
            throw new \CException("Autocreate error: ".json_encode($new->getErrors()));
        }
        return $new;
    }

    public function restoreByAttributes(array $attributes, $autoCreate = false) {
        
        $result = $this->owner->findByAttributes($attributes);
        
        if (!$result) {
            if ($autoCreate) $result = $this->createByAttributes($attributes);
        }

        return $result;
    }
}
