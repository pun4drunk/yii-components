<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModuleControllerBehavior
 *
 * @author vladislav
 */
namespace YiiComponents\behaviors;
use \CBehavior;

class ModuleControllerBehavior extends CBehavior {
    
    public $moduleId;
    protected $_module;
    
    public function getModule() {
        
        if (is_null($this->_module)) {
            $parent = \Yii::app();
            
            $ids = explode('/', $this->moduleId);
            foreach ($ids as $id) {
                $parent = $parent->getModule($id);
            }
            
            $this->_module = $parent;    
        }
        
        return $this->_module;
    }
    
}
