<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ActiveRecordCategorizedBehavior
 *
 * @author vladislav
 */
namespace YiiComponents\behaviors;

class ActiveRecordCategorizedBehavior extends ActiveRecordScopesBehavior {
    
    public $fieldName = 't.category';
    
    public function category($name) {
        $this->dbCriteria->compare($this->fieldName, $name);
        return $this->owner;
    }
    
}
