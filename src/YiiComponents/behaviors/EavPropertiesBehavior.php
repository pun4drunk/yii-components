<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DbSettingsBehavior
 *
 * @author vladislav
 */
namespace YiiComponents\behaviors;

class EavPropertiesBehavior extends DependentBehavior {
    
    public $category = 'eavproperties';
    
    public $entityAttr = 'entity';
    public $attributeAttr = 'attribute';
    public $valueAttr = 'value';
    
    public $entity;
    public $store = false;
    public $reset = false;
    public $show = false;
    public $attributes = array();
    public $modelClass;
    
    protected $loaded = array();
    protected $properties = array();
    protected $merged = array();
    
    
    protected function dependencies() {
        return \CMap::mergeArray(parent::dependencies(), array(
            'logger' => 'YiiComponents\behaviors\LoggerBehavior',
        ));   
    }
    
    public function merge() {
        
        foreach ($this->attributes as $attribute) {
            $this->merged[$attribute] = &$this->owner->$attribute;
        }
        
        $properties = \CActiveRecord::model($this->modelClass)->findAllByAttributes(array(
            $this->entityAttr => $this->entity,
            $this->attributeAttr => $this->attributes,
        ));
        
        foreach ($properties as $property) {
            
            $attribute = $property->{$this->attributeAttr};
            $value = $property->{$this->valueAttr};
            
            if ($this->reset) {
                $this->logger->addTrace("reset: {$attribute}", $this->category);
                $property->delete();
                continue;
            }
            
            $this->logger->addTrace("loaded: {$attribute} = {$value}", $this->category);
            $this->properties[$attribute] = $property;
            
            if (!strlen($this->merged[$attribute])) {
                $this->merged[$attribute] = $value;
                $this->loaded[$attribute] = $value;
            } else {
                $this->logger->addTrace("override: $attribute = '{$this->merged[$attribute]}'", $this->category);
            }
        }
        
        if ($this->store) {
            foreach (array_diff_assoc($this->merged, $this->loaded) as $attribute => $value) {
                
                $property = isset($this->properties[$attribute]) 
                         ? $this->properties[$attribute]
                         : new $this->modelClass;
                
                if (strlen($value)) {
                    
                    $property->attributes = array(
                        $this->entityAttr    => $this->entity,
                        $this->attributeAttr => $attribute,
                        $this->valueAttr     => $value,
                    );

                    $this->logger->addTrace("saving: $attribute = '$value'", $this->category);
                    $property->save();
                    
                } else if (!$property->isNewRecord){
                    $this->logger->addTrace("deleting: $attribute", $this->category);
                    $property->delete();
                }
                
                
            }
        }
        
        if ($this->show) {
            $this->logger->addInfo(json_encode($this->merged), $this->category);
        } else {
            $this->logger->addTrace(json_encode($this->merged), $this->category);
        }
    }
    
}
