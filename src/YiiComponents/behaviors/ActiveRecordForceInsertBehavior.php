<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ActiveRecordForceInsertBehavior
 *
 * @author vladislav
 */
namespace YiiComponents\behaviors;
use \CBehavior;

class ActiveRecordForceInsertBehavior extends CBehavior {
    
    public function forceInsert() {
        $this->owner->setIsNewRecord(true);
        return $this->owner->save();
    }
    
}
