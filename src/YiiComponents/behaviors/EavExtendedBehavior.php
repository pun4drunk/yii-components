<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EEavExtendedBehavior
 *
 * @author vladislav
 */
namespace YiiComponents\behaviors;
\Yii::import('eav.EEavBehavior');
use EEavBehavior;

class EavExtendedBehavior extends EEavBehavior {
    
    public function isUniqueValue($attribute, $value) {
        $count = (int)$this->getCountSameEavAttributesCommand($attribute, $value)->queryScalar();
        return !$count;
    }
    
    protected function getCountSameEavAttributesCommand($attribute, $value) {
        return $this->getOwner()
            ->getCommandBuilder()
            ->createCountCommand($this->tableName, $this->getLoadSameEavAttributeCriteria($attribute, $value));
    }
    
    protected function getLoadSameEavAttributeCriteria($attribute, $value) {
        $criteria = new \CDbCriteria;
        
        $criteria->addCondition("{$this->entityField} <> {$this->getModelId()}");
        $criteria->compare($this->attributeField, $attribute);
        $criteria->compare($this->valueField, $value);
        
        return $criteria;
    }
}
