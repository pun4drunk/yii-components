<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TimestampBehavior
 *
 * @author vladislav
 */
namespace YiiComponents\behaviors;

\Yii::import('zii.behaviors.CTimestampBehavior');
use \CTimestampBehavior;

class TimestampBehavior extends CTimestampBehavior{
    
    public function beforeSave($event) {
        
        if ($this->getOwner()->getIsNewRecord() && ($this->createAttribute !== null)) {
            if (empty($this->getOwner()->{$this->createAttribute})) 
                $this->getOwner()->{$this->createAttribute} = $this->getTimestampByAttribute($this->createAttribute);
        }
        
        if ((!$this->getOwner()->getIsNewRecord() || $this->setUpdateOnCreate) && ($this->updateAttribute !== null)) {
                $this->getOwner()->{$this->updateAttribute} = $this->getTimestampByAttribute($this->updateAttribute);
        }
    }
    
    public function getLastTimestamp($attribute = NULL) {
        $attribute = is_null($attribute) ? $this->createAttribute : $attribute;
        
        if (!$this->owner->hasAttribute($attribute)) {
            throw new \CException("attribute $attribute does not exist in ".get_class($this->owner));
        }
        
        return $this->owner->getDbConnection()->createCommand()
             ->select(new \CDbExpression("MAX($attribute)"))->from($this->owner->tableName())
             ->queryScalar();
        
    }
        
}
