<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ActiveRecordScopesBehavior
 *
 * @author vladislav
 */
namespace YiiComponents\behaviors;

class ActiveRecordScopesBehavior extends DependentBehavior {
    
    protected function dependencies() {
        return array(
            'dbConnection' => 'CDbConnection',
            'dbCriteria' => 'CDbCriteria',
        );
    }
    

}
