<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ActiveRecordTimedBehavior
 *
 * @author vladislav
 */
namespace YiiComponents\behaviors;
use YiiComponents\helpers\AppHelper;

class ActiveRecordTimedBehavior extends ActiveRecordScopesBehavior {
    
    public $timeField = 't.`created`';
    
    public function goBack($seconds, $from = NULL, $until = false) {
        
        $from = is_null($from) ? time() : $from;
        $threshold = AppHelper::getDbTimestampExpression($this->dbConnection, $from - $seconds);

        $this->dbCriteria->mergeWith(array(
            'condition'=> "$this->timeField ".($until ? "<" : ">")." $threshold",
        ));
        return $this->owner;
    }
    
    public function goBackUntil($seconds, $from = NULL) {
        return $this->goBack($seconds, $from, true);
    }
    
    
}
