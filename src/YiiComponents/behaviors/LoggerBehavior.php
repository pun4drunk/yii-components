<?php 
namespace YiiComponents\behaviors;
use CBehavior;

class LoggerBehavior extends CBehavior {

    public $category = "application";
    public $live = false;
    public $debug = true;
    
    public $dateFormat = 'H:i:s';
    public $liveMask = "[{date}][{category}][{level}] {message} \n";
    public $liveMaskDebug = "[{date}][{category}][{level}] {message} {{ownerClass}}\n";
    public $requireCategory = true;
    public $debugOwner = false;
    
    public function attach($owner){

        if (is_null($this->debug)) {
            $this->debug = YII_DEBUG;
        }
        
        parent::attach($owner);
    }

    public function addLog($message, $level = 'info', $category = NULL) {
        
        if ($this->requireCategory && is_null($category)) {
            throw new \CException("category required but not specified");
        }
        
        if ($category) {
            $category = "$this->category.$category";
        } else {
            $category = $this->category;
        }
        
        if ($trace = \CLogger::LEVEL_TRACE === $level) {
            \Yii::trace($message, $category);
        } else {
            \Yii::log($message, $level, $category);
        }
        
        if ($this->live && (!$trace || $this->debug)) {
            echo $this->createLiveMessage($message, $category, $level, time());
        }
        
        return $this;
    }
    
    public function createLiveMessage($message, $category, $level, $time = NULL) {
        
        $time = is_null($time) ? time() : $time;
        
        return strtr($this->debug ? $this->liveMaskDebug : $this->liveMask, array(
            '{date}'        => date($this->dateFormat, $time),
            '{category}'    => $category,
            '{level}'       => strtoupper($level),
            '{message}'     => $message,
            '{ownerClass}'  => get_class($this->owner),
        ));
    }
    
    public function addInfo($message, $category = NULL) {
        return $this->addLog($message, \CLogger::LEVEL_INFO, $category);
    }
    
    public function addError($message, $category = NULL) {
        return $this->addLog($message, \CLogger::LEVEL_ERROR, $category);
    }
    
    public function addWarning($message, $category = NULL) {
        return $this->addLog($message, \CLogger::LEVEL_WARNING, $category);
    }
    
    public function addTrace($message, $category = NULL) {
        return $this->addLog($message, \CLogger::LEVEL_TRACE, $category);
    }

}