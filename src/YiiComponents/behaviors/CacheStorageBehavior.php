<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of CacheStorageBehavior
 *
 * @author vladislav
 */
namespace YiiComponents\behaviors;
use YiiComponents\interfaces\IStorageBehavior;

class CacheStorageBehavior extends CacheBehavior implements IStorageBehavior {
    
    public function reset($name) {
        return $this->delete($name);
    }
    
}
