<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DbTableMigrationBehavior
 *
 * @author vladislav
 */
namespace YiiComponents\behaviors;

use YiiComponents\helpers\AppHelper;

class DbTableMigrationBehavior extends DependentBehavior {
    
    const ACTION_CREATE = 'create';
    const ACTION_DROP = 'drop';
    const ACTION_SYNC = 'sync';
   
    
    public $tableName;
    public $tableParams;
    public $postCreateFx;
    public $autoUpgrade = false;
    
    protected function dependencies() {
        return array(
            'dbConnection' => '\CDbConnection',
        );    
    }
    
    public function create() {
        
        if (is_null($this->dbConnection->schema->getTable($this->tableName, true))) 
        {
            $this->owner->createTable($this->tableName, $this->tableParams);

            if (!is_null($this->postCreateFx)) {
                echo "table `$this->tableName` post create fx is '$this->postCreateFx'", PHP_EOL;
                call_user_func(array($this->owner, $this->postCreateFx));
            }
            
        } else {
            
            echo "connectionString is {$this->dbConnection->connectionString}", PHP_EOL;
            echo "table `$this->tableName` exists", PHP_EOL;
            
            if ($this->autoUpgrade) {
                return $this->upgrade();
            }
        }
        
        return true;
    }
    
    public function drop() {
        
        $table = $this->dbConnection->schema->getTable($this->tableName, true);
        if (!is_null($table)) {
            $this->owner->dropTable($this->tableName);
        } 
        
        return true;
    }
    
    protected function getTableColumns() {
        $columns = array();
        foreach ($this->tableParams as $columnName => $columnParams) {
            if(!is_numeric($columnName)) {
                array_push($columns, $columnName);
            }
        }
        return $columns;
    }
    
    public function upgrade() {
        return $this->sync();
    }
    
    public function sync() {
            
        if (!$table = $this->dbConnection->schema->getTable($this->tableName, true)) {
            return $this->create();
        }
        
        $columnsOld = array_keys($table->columns);
        $columnsNew = $this->tableColumns;
        
        //throw exception if new table is too narrow
        if ($old = array_diff($columnsOld, $columnsNew)) {
            throw new \CException("can't sync: old data columns will be lost: ".json_encode($old));
        }
        
        foreach ($columnsOld as $columnName) {
            
            $columnParams = $this->tableParams[$columnName];
            //fix NOT NULL
            if (preg_match("/.+ NOT NULL DEFAULT '?(\w+)'?/", $columnParams, $matches)) {
                $this->owner->execute("UPDATE `$this->tableName` SET `$columnName` = {$matches[1]} WHERE `$columnName` IS NULL");
            }    
        }

        $oldTableName = "_{$this->tableName}_old_".time();
        
        $this->owner->renameTable($this->tableName, $oldTableName);
        $this->owner->createTable($this->tableName, $this->tableParams);

        $fields = AppHelper::implodeWrap(',', $columnsOld, '`');
        $sql = "INSERT OR REPLACE INTO `$this->tableName` ($fields) "
                . "SELECT $fields FROM `$oldTableName`";
        $this->owner->execute($sql);

        $this->owner->dropTable($oldTableName);
        
        return true;
    }
    
}
