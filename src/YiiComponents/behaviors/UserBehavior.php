<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

namespace YiiComponents\behaviors;
use CBehavior;

class UserBehavior extends CBehavior {
    
    public $userClass = 'User';
    public $user_id;
    public $username;
    public $attrUsername = 'username';
    
    protected $user;
    
    public function getUser($force = false) {
        
        if (!$this->userClass) {
            throw new \CException("User Class must be set", 500);
        }
        
        $userClass = $this->userClass;
        
        if (is_null($this->user) || $force) {
            
            if ($this->user_id) {
            
                $user = \CActiveRecord::model($this->userClass)->findByPk($this->user_id);

                if (!$user) {
                    throw new \CException("User #$this->user_id does not exist");
                }

            } else if ($this->username) {

                $user = \CActiveRecord::model($this->userClass)->findByAttributes(array(
                    $this->attrUsername => $this->username,
                ));

                if (!$user) {
                    throw new \CException("User '$this->username' does not exist");
                }

            } else {
                throw new \CException("Neither --user_id nor --{$this->attrUsername} where specified");
            }
            
            $this->user = $user;
            
        }
        
        return $this->user;
    }
    
    public function getUsername() {
        return $this->user ? $this->user->{$this->attrUsername} : NULL;
    }
    
    public function getUserId() {
        return $this->user ? $this->user->primaryKey : NULL;
    }
    
}
