<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of DependedBehavior
 *
 * @author vladislav
 */
namespace YiiComponents\behaviors;
use \CBehavior;

class DependentBehavior extends CBehavior {
    
    public $graceful = false;
    
    protected function dependencies() {
        return array(
            //'name' => 'class',
        );
    }
    
    public function attach($owner) {

        parent::attach($owner);
        
        foreach ($this->dependencies() as $name => $class) {
        
            try{
                $behavior = $this->owner->$name;
            } catch (\Exception $e) {
                if (!$this->graceful) {
                    throw $e;
                }
                throw new \CException(get_class($this->owner)." does not have the property/behavior '$name' dependent by ".get_class($this).": ".$e->getMessage());
            }
            
            if (!$behavior instanceof $class) {
                throw new \CException(get_class($this->owner)."'s property '$name' dependent by ".get_class($this).", is not an instance of required class '$class'");
            }
        }
                
    }
    
    public function __get($name) {
        $dependencies = $this->dependencies();
        if (isset($dependencies[$name])) {
            return $this->owner->$name;
        }
        return parent::__get($name);
    }
    
}
