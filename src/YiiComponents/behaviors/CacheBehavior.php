<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of CacheBehavior
 *
 * @author vladislav
 */
namespace YiiComponents\behaviors;
use CBehavior;

class CacheBehavior extends CBehavior {
    
    protected $cache = NULL;
    
    public $cacheRequired = true;
    public $cacheKeyPrefix = 'app.cache';
    public $cacheId = 'cache';
    public $cacheExpire = 86400;
    
    public function attach($owner) {
        // Prepare cache component.
        $this->cache = \Yii::app()->getComponent($this->cacheId);
        
        if (!($this->cache instanceof \ICache)) {
            
            if ($this->cacheRequired) {
                throw new \CException("Please enable cache to use '".get_class($this)."' behavior");
            } else {
                $this->cache = new \CDummyCache;
            }
        }
        // Call parent method for convenience.
        parent::attach($owner);
    }
    
    public function set($key, $value, $expire = NULL, $dependency = NULL) {
        if (is_null($expire)) {
            $expire = $this->cacheExpire;
        }
        return $this->cache->set("$this->cacheKeyPrefix.$key", $value, $expire, $dependency);
    }
    
    public function get($key) {
        return $this->cache->get("$this->cacheKeyPrefix.$key");
    }
    
    public function delete($key) {
        return $this->cache->delete("$this->cacheKeyPrefix.$key");
    }
    
    public function extract($key) {
        
        if ($value = $this->get($key)) {
            $this->delete($key);
        }
        return $value;
    }
    
    public function setGlobalState($key, $value) {
        return Yii::app()->setGlobalState("$this->cacheKeyPrefix.$key", $value);
    }
    
    public function getGlobalStateDependency($key) {
        return new CGlobalStateCacheDependency("$this->cacheKeyPrefix.$key");
    }

    
}
